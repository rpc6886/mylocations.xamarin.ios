﻿using System;
using System.Linq;
using CoreLocation;
using Foundation;
using UIKit;

namespace mylocations.xamarin.ios
{
	public partial class CurrentLocationViewController : UIViewController
	{
		private readonly CLLocationManager _locationManager;
		private CLLocation _currentLocation;
		private NSError _lastLocationError;
		private bool _isLocationBeingUpdated = false;
		private CLGeocoder _geocoder;
		private CLPlacemark _placemark;
		private bool _isPeformingReverseGeocoding = false;
		private NSError _lastGeocodingError;
		private NSTimer _timer;

		public CurrentLocationViewController (IntPtr handle) : base (handle)
		{
			_locationManager = new CLLocationManager ();
			_geocoder = new CLGeocoder ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			GetButton.TouchUpInside += (o, s) => GetLocation ();
			_locationManager.Failed += (object sender, Foundation.NSErrorEventArgs e) => OnLocationFailedWithError (e);
			_locationManager.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) => OnLocationUpdated (e);

			TagButton.TouchUpInside += (sender, e) => {
				
			};

			UpdateLabels ();
			ConfigureGetButton ();
		}

		private void OnLocationFailedWithError (Foundation.NSErrorEventArgs e)
		{
			Console.WriteLine (string.Format ("didFailWithError {0}", e.Error));	

			if (e.Error.Code == (int)CLError.LocationUnknown) {
				return;
			}

			_lastLocationError = e.Error;
			StopLocationManager ();
			UpdateLabels ();
			ConfigureGetButton ();
		}

		private void OnLocationUpdated (CLLocationsUpdatedEventArgs e)
		{
			var mostRecentLocation = e.Locations.OrderByDescending (x => x.Timestamp).First ();

			if (mostRecentLocation.Timestamp.SecondsSinceReferenceDate < -5) {
				return;
			}

			if (mostRecentLocation.HorizontalAccuracy < 0) {
				return;
			}

			var distance = CLLocationDistance.MaxDistance;

			if (_currentLocation != null) {
				distance = mostRecentLocation.DistanceFrom (_currentLocation);
			}

			if (_currentLocation == null || _currentLocation.HorizontalAccuracy > mostRecentLocation.HorizontalAccuracy) {
				_lastLocationError = null;
				_currentLocation = mostRecentLocation;
				UpdateLabels ();

				if (mostRecentLocation.HorizontalAccuracy <= _locationManager.DesiredAccuracy) {
					StopLocationManager ();
					ConfigureGetButton ();

					if (distance > 0) {
						_isPeformingReverseGeocoding = false;
					}
				}
			}

			if (!_isPeformingReverseGeocoding) {
				Console.WriteLine ("*** Going to geocode");

				_isPeformingReverseGeocoding = true;

				_geocoder.ReverseGeocodeLocation (mostRecentLocation, (CLPlacemark[] placemarks, NSError error) => {
					//Console.WriteLine (string.Format ("*** Found placemarks: {0}, error: {1}", placemarks, error));

					this._lastGeocodingError = error;

					if (error == null) {
						this._placemark = placemarks.Last ();
					} else {
						this._placemark = null;
					}

					this._isPeformingReverseGeocoding = false;
					this.UpdateLabels ();
				});
			} else if (distance < 1.0) {
				var timeInterval = ((DateTime)mostRecentLocation.Timestamp - (DateTime)_currentLocation.Timestamp).Seconds;

				if (timeInterval > 10) {
					Console.WriteLine ("*** Force done!");
					StopLocationManager ();
					UpdateLabels ();
					ConfigureGetButton ();
				}
			}
		}

		private void GetLocation ()
		{
			var currentAuthorizationStatus = CLLocationManager.Status;

			if (currentAuthorizationStatus == CLAuthorizationStatus.NotDetermined) {
				_locationManager.RequestWhenInUseAuthorization ();
				return;
			}

			if (currentAuthorizationStatus == CLAuthorizationStatus.Denied || currentAuthorizationStatus == CLAuthorizationStatus.Restricted) {
				ShowLocationServicesDeniedAlert ();
				return;
			}

			if (_isLocationBeingUpdated) {
				StopLocationManager ();
			} else {
				_currentLocation = null;
				_lastLocationError = null;
				_placemark = null;
				_lastGeocodingError = null;
				StartLocationManager ();
			}

			UpdateLabels ();
			ConfigureGetButton ();
		}

		private void UpdateLabels ()
		{
			if (_currentLocation != null) {
				LatitudeLabel.Text = _currentLocation.Coordinate.Latitude.ToString ("0.00000000");
				LongitudeLabel.Text = _currentLocation.Coordinate.Longitude.ToString ("0.00000000");
				TagButton.Hidden = false;
				MessageLabel.Text = "";

				if (_placemark != null) {
					AddressLabel.Text = StringFromPlacemark (_placemark);
				} else if (_isPeformingReverseGeocoding) {
					AddressLabel.Text = "Search for Address...";
				} else if (_lastGeocodingError != null) {
					AddressLabel.Text = "Error Finding Address";
				} else {
					AddressLabel.Text = "No Address Found";
				}
				return;
			}

			LatitudeLabel.Text = "";
			LongitudeLabel.Text = "";
			AddressLabel.Text = "";
			TagButton.Hidden = true;

			var statusMessage = "";

			if (_lastLocationError != null) {
				if (_lastLocationError.Domain == NSError.CoreLocationErrorDomain && _lastLocationError.Code == (int)CLError.Denied) {
					statusMessage = "Location Services Disabled";
				} else {
					statusMessage = "Error Getting Location";
				}
			} else if (!CLLocationManager.LocationServicesEnabled) {
				statusMessage = "Location Services Disabled";
			} else if (_isLocationBeingUpdated) {
				statusMessage = "Searching...";
			} else {
				statusMessage = "Tap 'Get My Location' to Start";
			}

			MessageLabel.Text = statusMessage;
		}

		private static string StringFromPlacemark (CLPlacemark placemark)
		{
			return placemark.SubThoroughfare + " " + placemark.Thoroughfare + " " + "\n" +
			placemark.Locality + " " + placemark.AdministrativeArea + " " + placemark.PostalCode;
		}

		private void ConfigureGetButton ()
		{
			if (_isLocationBeingUpdated) {
				GetButton.SetTitle ("Stop", UIControlState.Normal);
				return;
			}

			GetButton.SetTitle ("Get My Location", UIControlState.Normal);
		}

		private void StartLocationManager ()
		{
			if (CLLocationManager.LocationServicesEnabled) {
				_locationManager.DesiredAccuracy = 10; // In Meters
				_locationManager.StartUpdatingLocation ();
				_isLocationBeingUpdated = true;

				_timer = NSTimer.CreateScheduledTimer (new TimeSpan (60), (timer) => {
					Console.WriteLine ("*** Time out");	

					if (_currentLocation == null) {
						StopLocationManager ();
						_lastLocationError = new NSError (new NSString ("MyLocationsErrorDomain"), 1);
						UpdateLabels ();
						ConfigureGetButton ();
					}
				});
			}
		}

		private void StopLocationManager ()
		{
			if (_isLocationBeingUpdated) {
				if (_timer != null) {
					_timer.Invalidate ();
				}

				_locationManager.StopUpdatingLocation ();
				_isLocationBeingUpdated = false;
			}
		}

		private void ShowLocationServicesDeniedAlert ()
		{
			// TODO: Add Alert
		}
	}
}

