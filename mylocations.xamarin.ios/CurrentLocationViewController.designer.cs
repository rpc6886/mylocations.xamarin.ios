// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace mylocations.xamarin.ios
{
	[Register ("CurrentLocationViewController")]
	partial class CurrentLocationViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel AddressLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton GetButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel LatitudeLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel LongitudeLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel MessageLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton TagButton { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (AddressLabel != null) {
				AddressLabel.Dispose ();
				AddressLabel = null;
			}
			if (GetButton != null) {
				GetButton.Dispose ();
				GetButton = null;
			}
			if (LatitudeLabel != null) {
				LatitudeLabel.Dispose ();
				LatitudeLabel = null;
			}
			if (LongitudeLabel != null) {
				LongitudeLabel.Dispose ();
				LongitudeLabel = null;
			}
			if (MessageLabel != null) {
				MessageLabel.Dispose ();
				MessageLabel = null;
			}
			if (TagButton != null) {
				TagButton.Dispose ();
				TagButton = null;
			}
		}
	}
}
